/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.th.nnl.invoice.test

import co.th.nnl.invoice.util.InvoicePerferences

/**
 *
 * @author Thanachai.t
 */
class AllUtilTest {
    void testInvoicePerferences() {
        InvoicePerferences.set("testKey", "testValue")
        assertEquals("testValue", InvoicePerferences.get("testKey"))
        
        InvoicePerferences.setThis("testKey", "testValue")
        assertEquals("testValue", InvoicePerferences.getThis("testKey"))
    }
    
    void testToBathText() {    
        assertEquals("ศูนย์บาทถ้วน", InvoiceUtil.toBathText("0"));
        assertEquals("หนึ่งบาทถ้วน", InvoiceUtil.toBathText("1"));
        assertEquals("สิบบาทถ้วน", InvoiceUtil.toBathText("10"));
        assertEquals("สิบเอ็ดบาทถ้วน", InvoiceUtil.toBathText("11"));
        assertEquals("สิบสองบาทถ้วน", InvoiceUtil.toBathText("12"));
        assertEquals("ยี่สิบบาทถ้วน", InvoiceUtil.toBathText("20"));
        assertEquals("ยี่สิบเอ็ดบาทถ้วน", InvoiceUtil.toBathText("21"));
        assertEquals("ยี่สิบสามบาทถ้วน", InvoiceUtil.toBathText("23"));
        assertEquals("หนึ่งร้อยบาทถ้วน", InvoiceUtil.toBathText("100"));
        assertEquals("หนึ่งพันหนึ่งบาทถ้วน", InvoiceUtil.toBathText("1001"));
        assertEquals("สองหมื่นสองร้อยยี่สิบบาทถ้วน", InvoiceUtil.toBathText("20220"));
        assertEquals("ห้าแสนหกหมื่นเจ็ดพันหนึ่งร้อยสามสิบสองบาทถ้วน", InvoiceUtil.toBathText("567132"));
        assertEquals("หนึ่งล้านล้านสองหมื่นสามพันหกร้อยเก้าสิบแปดล้านเจ็ดแสนห้าหมื่นหนึ่งพันยี่สิบสี่บาทถ้วน", InvoiceUtil.toBathText("1023698751024"));
    }
}

