/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.th.nnl.invoice.test.util

import co.th.nnl.invoice.bean.*;
import co.th.nnl.invoice.bean.Customer
import co.th.nnl.invoice.bean.Invoice

/**
 *
 * @author Thanachai.t
 */
public class SimpleData {
    private static List<Customer> customers = new LinkedList<>();
    private static List<Invoice> invoices = new LinkedList<>();
    
    static {
        customers << new Customer('บริษัท ซี.พี.คอนซูเมอร์ โพร์ดักส์ จำกัด', 'เลขที่ 36 ถนนรามอินทรา แขวงมีนบุรี เขตมีนบุรี กรุงเทพฯ 10510')
        customers << new Customer('บริษัท  แชมป์เวย์ เอเชีย แลนด์บริดจ์ จำกัด', '2922/132 อาคารชาญอิสสระทาวเวอร์ 2  ชั้น 3 ถ.เพชรบุรีตัดใหม่  แขวงบางกระปิ  เขตห้วยขวาง กรุงเทพ ฯ  10320')
        
//        invoices << new Invoice(            
//            'BOOKING NO.งาน SHIN PAPER', 
//            '18/10/2015', 
//            'TAX ID : 0105541069148   ( สำนักงานใหญ่  )    ', 
//            'NT2015/10001', 
//            ['8/19/1958', '', '8/20/1958'], 
//            [
//                'ค่าขนส่ง UNITHAI  -  บางแค -   YJC           1X40\'HQ.', 
//                'ค่าขนส่ง PAT   -  บางแค -   YJC                  2X40\'HQ.', 
//                'ค่าคืนตู้เปล่า ',
//                '1.SKHU - 8717223',
//                '2.GATU - 8738047',
//                '3.SKHU - 8701675'
//            ], 
//            ['1', '1', '2', '2', '3', '3'], 
//            ['5500', '976', '5000', '976', '1234', '5678'])
    }   
        
    public static getCustomerList() {
        return customers
    }
    public static Customer getCustomer(index) {
        return customers.get(index)
    }     
    
    public static getInvoice(index) {
        return invoices[index]
    }
}

