package co.th.nnl.invoice.test.unit

import co.th.nnl.invoice.handle.CustomerHandle
import co.th.nnl.invoice.test.util.SimpleData
import co.th.nnl.invoice.util.*
import org.bson.types.ObjectId
import org.junit.BeforeClass

import co.th.nnl.invoice.bean.Customer
/**
 *
 * @author Thanachai.t
 */
class CustomerTest extends GroovyTestCase {
    private CustomerHandle customerHandle
    
    {
        Connection.createConnection("mongodb://localhost/nnl_dev");
        customerHandle = new CustomerHandle()
    }   
    
    void testHandle() {    
        SimpleData.getCustomerList().each({ c ->   
                ObjectId id = customerHandle.save(c)        
                assertNotNull id        
                assertEquals(c, customerHandle.findById(id))
                assertTrue customerHandle.remove(id)   
            })
        
    }
}