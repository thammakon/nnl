/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.th.nnl.invoice.handle

import com.mongodb.BasicDBObject
import com.mongodb.DBCollection
import com.mongodb.MongoException
import org.bson.types.ObjectId
import co.th.nnl.invoice.util.Connection

/**
 *
 * @author Thanachai.t
 */
public abstract class HandleBean<T> {
    // get collection bean name
    protected DBCollection DB
    
    {
        if (DB == null) {
            DB = Connection.getCollection(this.getClass().getSimpleName().split('Handle')[0])
        }
    }
    
    public ObjectId save(T t) throws MongoException {
        assert t != null
        
        log.info 'Save: {}', t.toString().hashCode()
        log.debug 'Save: {}', t
        
        DB.save(t)      
        
        log.info 'Save _id: {}', t.getId()
        return t.getId()
    }
    
    public findAll() {
        def result = []
        DB.find().each({
                result << it
            })
        return result
    }
    
    public T findById(ObjectId id) {
        return DB.findOne(new BasicDBObject('_id', id))
    }
    
    public boolean remove(ObjectId id) {
        log.info 'Remove: {}', id
        DB.remove(new BasicDBObject('_id', id))
        return DB.findOne(new BasicDBObject('_id', id)) == null
    }
    
    public boolean remove(T t) {
        remove(t.getId())
    }
}

