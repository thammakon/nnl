/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.th.nnl.invoice.handle

import co.th.nnl.invoice.bean.Customer
import co.th.nnl.invoice.handle.HandleBean
import co.th.nnl.invoice.bean.Invoice
import co.th.nnl.invoice.util.*
import com.mongodb.*
import com.mongodb.BasicDBList
import com.mongodb.BasicDBObject
import com.mongodb.MongoException
import com.mongodb.util.JSON
import groovy.util.logging.Slf4j
import java.security.PrivilegedActionException
import java.text.*
import java.util.Date
import java.util.regex.Pattern
import org.bson.types.ObjectId
/**
 * 
 * invoice = [id, invoiceDate, remark, sellId, itemId, itemDescription, itemQuantity, itemUnitPrice]
 *
 * @author Thanachai.t
 */


@Slf4j
class InvoiceHandle extends HandleBean<Invoice> {    
    
    public String getNextInvoiceId() {
        int n = 0
        def sdf = new SimpleDateFormat("yyyy/M");
        def qStr = "^NT" + sdf.format(new Date())
        BasicDBObject basicDBObject = new BasicDBObject("invoiceId", java.util.regex.Pattern.compile(qStr, Pattern.CASE_INSENSITIVE))
        def query = DB.find(basicDBObject).sort(new BasicDBObject('invoiceId', -1)).limit(1)
        if (query != null) {
            query.each({
                    n = Integer.parseInt(it.invoiceId[-3..-1])
                })   
        } 
        n++
        DecimalFormat myFormatter = new DecimalFormat('000')
        return myFormatter.format(n);
    }
    
    public static void main(String[] args) {
        new InvoiceHandle().getNextInvoiceId()
    }
}