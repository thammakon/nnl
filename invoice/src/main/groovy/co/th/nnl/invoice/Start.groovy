/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.th.nnl.invoice

import co.th.nnl.invoice.util.InvoiceUtil
import java.util.regex.Pattern.Start
import javax.swing.UIDefaults
import javax.swing.UIManager

import co.th.nnl.invoice.ui.*
import co.th.nnl.invoice.ui.ManageDbDialog
import co.th.nnl.invoice.util.InvoicePerferences

/**
 *
 * @author Thanachai.t
 */
class Start {	 
    private static java.util.prefs.Preferences prefs = java.util.prefs.Preferences.userRoot()
    
    private startLocalDB() {
        def p = Runtime.getRuntime().exec('D:\\Databases\\MongoDB\\Server\\3.0\\start.bat')
    }
        
    public static void main(String[] args) { 
        if (prefs.getBoolean("isModeDev", false)) {
            println 'Dev Mode...'
            new Start().startLocalDB()
            //            prefs.remove("mongoURI")
            prefs.remove("historySellId")
            prefs.remove("historyRemark")
            //            InvoicePerferences.remove("noDb")
        } 
        
        new Start().startUI()
    }    
    
    private void startUI() {          
        try {
            javax.swing.UIManager.setLookAndFeel(
                InvoicePerferences.get(
                    "lookAndFeel",
                    javax.swing.UIManager.getSystemLookAndFeelClassName()
                )
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                    InvoiceFrame.getInstance().setVisible(true)     
                }
            });              
    }
}