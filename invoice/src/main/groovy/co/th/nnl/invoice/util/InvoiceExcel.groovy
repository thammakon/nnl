/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.th.nnl.invoice.util

import co.th.nnl.invoice.bean.Customer
import co.th.nnl.invoice.bean.Invoice
import co.th.nnl.invoice.util.InvoiceUtil
import groovy.util.logging.Slf4j
import java.awt.Desktop
import java.awt.print.PrinterJob
import java.io.*
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.print.*
import javax.print.Doc
import javax.print.DocFlavor
import javax.print.DocPrintJob
import javax.print.PrintService
import javax.print.SimpleDoc
import javax.print.attribute.*
import javax.print.attribute.DocAttributeSet
import javax.print.attribute.HashDocAttributeSet
import org.apache.poi.hssf.usermodel.HSSFCell
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.DataFormat
/**
 *
 * @author Thanachai.t
 */
@Slf4j
class InvoiceExcel {
    private static URL resource = new String().getClass().getResource("/template/invoice.xls")
    private static File templateFile = new File(resource.toURI())
    private resultFile
    private static Desktop desktop = Desktop.getDesktop()
    private static PrintService service    
    
    //For debug
    public static void main(String[] arg) {
        service = PrintServiceLookup.lookupDefaultPrintService()
    }
    
    InvoiceExcel(Invoice invoice, Customer customer) {        
        log.info('Data: {}', invoice.hashCode())
        resultFile = new File(System.getProperty("java.io.tmpdir") + '\\NNL\\Invoice\\'+ new Date().getTime() + '\\' + invoice.getInvoiceId() + '.xls')
        resultFile.getParentFile().mkdirs()
        
        preparedstatement(invoice, customer)
    }
        
    public void print() {      
        service = PrintServiceLookup.lookupDefaultPrintService()
        
        // Check Visual Printer or Fax
        PrintServiceAttributeSet attributes = service.getAttributes();
        DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE
        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet()
        
        for (Attribute attr : attributes.toArray()) {
            if (attr.getName() == "printer-name" && 
                (attr.toString() == "Adobe PDF") || (attr.toString() == "Fax") || (attr.toString() == "Microsoft XPS Document Writer")) {                
                //                desktop.print(resultFile)                
                service = ServiceUI.printDialog(null, 400, 400, PrintServiceLookup.lookupPrintServices(flavor, pras), PrintServiceLookup.lookupDefaultPrintService(), flavor, pras);
                return;
            }
        }
        
        // Do when use Hardware Printer
        DocPrintJob job = service.createPrintJob()
        DocAttributeSet das = new HashDocAttributeSet();
        Doc doc = new SimpleDoc(new FileInputStream(resultFile), flavor, das)
        job.print(doc, pras)
    }
        
    public open() {    
        desktop.open(resultFile)
    }
    
    private preparedstatement(Invoice invoice, Customer customer) {
        def data = invoice.toMap()
        
        data['customer'] = customer.toMap()
        
        data['amount'] = []
        
        0.upto(9, { i ->
                if (data['itemQuantity'][i] != null && data['itemQuantity'][i] != '')
                data['amount'] << Integer.parseInt(data['itemQuantity'][i]) + Integer.parseInt(data['itemUnitPrice'][i])
            })
        
        
    
        data['subTotal'] = data['amount'].sum()
        data['grandTotal'] =  data['subTotal']
        data['totalBahtText'] =  InvoiceUtil.toBathText(data['subTotal'].toString())
        
        
        
        File file = templateFile
        FileInputStream fIP = new FileInputStream(file)
        HSSFWorkbook workbook = new HSSFWorkbook(fIP)
        def spreadsheet = workbook.getSheetAt(0)
        
        DataFormat format = workbook.createDataFormat();
        
        spreadsheet.each({ row ->
                row.each({ cell ->
                        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                            String c = cell.getStringCellValue()
                            Matcher m = Pattern.compile(/\$\{[(\w||\.)\[\]]*\}/).matcher(c)
                            while(m.find()) {
                                def varName = m.group(0)[2..-2]
                                def newC
                                if (varName[-1] != ']') {
                                    // exp.   ${a}, ${b}
                                    if (varName.split(/\./).size() == 1) {
                                        try {
                                            newC = c.replaceAll('\\$\\{' + varName + '\\}', (data[varName] == null) ? '':data[varName].toString())
                                        } catch (All) {
                                            newC = c.replaceAll('\\$\\{' + varName + '\\}', '')
                                        }
                                    } else {
                                        // exp. ${a,b}
                                        def a = varName.split(/\./)[0]
                                        def b = varName.split(/\./)[1]
                                        try {
                                            newC = c.replaceAll('\\$\\{' + varName + '\\}', (data[a][b] == null) ? '':data[a][b]).toString()
                                        } catch (All) {
                                            newC = c.replaceAll('\\$\\{' + varName + '\\}', '')
                                        }
                                    }
                                } else {
                                    // exp.   ${a[1]}, ${b[2]}
                                    def val = (data[varName[0..-4]] != null && data[varName[0..-4]][varName[-2] as int] != null) ? data[varName[0..-4]][varName[-2] as int]:'' 
                                    newC = c.replaceAll('\\$\\{' + varName[0..-4] + '\\['  + varName[-2] + '\\]\\}', val.toString())
                                }
                                
                                if (newC.isNumber())
                                newC = newC as BigInteger
                                cell.setCellValue(newC)
                            }
                        }
                    })
            })        
        
        try {
            workbook.getCreationHelper()
            .createFormulaEvaluator()
            .evaluateAll();
        } catch (Exception e) {log.error('{}', e.stackTrace)}

        
        
        FileOutputStream out = new FileOutputStream(resultFile)
        workbook.write(out);
        out.close();                
    }        
}