package co.th.nnl.invoice.util

import com.mongodb.BasicDBObject
import com.mongodb.DB
import com.mongodb.DBCollection
import com.mongodb.Mongo
import com.mongodb.MongoClient
import com.mongodb.MongoClientURI
import com.mongodb.MongoException
import com.mongodb.MongoURI
import groovy.util.logging.Slf4j
import org.bson.Document
import org.codehaus.groovy.tools.shell.util.Preferences

@Slf4j
public class Connection {

    private collection
    private static MongoClient mongo
    private static db
    private static java.util.prefs.Preferences prefs = java.util.prefs.Preferences.userRoot()

    private Connection() {}

    /**
    @param Collection Name
     */
    public static DBCollection getCollection(String name) {
        if (db == null) {
            new Connection().createConnection()
        }
        return db.getCollection(name)
    }
    
    public static void clearConnection() {
        db = null
    }
    
    public void createConnection() throws Exception {        
        String strURI = prefs.get("mongoURI", null)
        if (strURI != null) {
            MongoClientURI uri = new MongoClientURI(strURI)
                
            log.info("Mongo     Host: {}", uri.getHosts())
            log.info("Mongo Username: {}", uri.getUsername())
            log.debug("Mongo Password: {}", uri.getPassword())
            log.info("Mongo Database: {}", uri.getDatabase())
        
            mongo = new MongoClient(uri)
            db = mongo.getDB(uri.getDatabase())     
        }
    }     
    
    public static void createConnection(String strURI) throws Exception {  
        if (strURI != null) {
            MongoClientURI uri = new MongoClientURI(strURI)
        
            mongo = new MongoClient(uri)
            db = mongo.getDB(uri.getDatabase())     
        }
    }      
    
    private static void initDatabase() {
        //  Create Index
        db.getCollection("Customer").createIndex(new BasicDBObject("name", 1), new BasicDBObject("unique", true))
        db.getCollection("Customer").createIndex(new BasicDBObject("address", 1), new BasicDBObject("unique", true))
        
        db.getCollection("Invoice").createIndex(new BasicDBObject("invoiceId", 1), new BasicDBObject("unique", true))
    }
    
    public static void changeDatabase(String mongoURI) throws Exception {
        MongoClientURI uri = new MongoClientURI(mongoURI as String)  
        MongoClient mongoTest = new MongoClient(uri)
		
        // Test Connection
        mongoTest.getDB(uri.getDatabase()).command("ping")

        if (mongoTest != null && mongo != null && mongoTest.getAddress() != mongo.getAddress()) {
            mongo.close()
        }
        
        prefs.put("mongoURI", mongoURI)
        new Connection().createConnection()
        Connection.initDatabase()
    }
    
    /**
    For test
     */
    public static void main(String[] args) {
        Connection.getCollection('Invoice')
        Connection.initDatabase()
    }
}