/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.th.nnl.invoice.util

import com.mongodb.DBObject
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j

/**
 *
 * @author Thanachai.t
 */
@Slf4j
class ErrorHandle {
    public static String getErrorMessage(Exception ex) {
        ex.printStackTrace()
        def exType = ex.getClass().getName()
        if (exType == 'com.mongodb.MongoException$DuplicateKey') {
            def jsonSlurper = new JsonSlurper()
            return 'ข้อมูลซ้ำซ้อน\n' + jsonSlurper.parseText(ex.message)['err']
        } else {
            return ex.message
        }
        new ErrorHandle().writeLog(ex)
    }
    
    private writeLog(Exception ex) {
        println ex
    }
    
    public static void main(String[] args) {
        try {
            false
        } catch (All) {
         //   println this.getErrorMessage(All)
        }
    }
}

