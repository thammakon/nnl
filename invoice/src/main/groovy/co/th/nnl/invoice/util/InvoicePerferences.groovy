/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.th.nnl.invoice.util

import java.lang.invoke.MethodHandleImpl.BindCaller.T
import java.util.prefs.Preferences

/**
 *
 * @author Thanachai.t
 */
class InvoicePerferences {
    
    private static Preferences prefs = Preferences.userNodeForPackage(this.getClass());
    private static Preferences prefsRoot = Preferences.userRoot();
    
    static {
    }
    
    // Boolean Perferences    
    public static Object getObject(String key) {
        return deserialize(prefsRoot.getByteArray(key))
    }
    
    public static Object getThisObject(String key) {
        Class thisClass = new Exception().getStackTrace()[1].getClassName();
        return deserialize(Preferences.userNodeForPackage(thisClass).getByteArray(key))
    }
    
    public static void setObject(String key, Object value) {
        prefsRoot.putByteArray(key, serialize(value))
    }
    
    public static void setThisObject(String key, Object value) {
        Class thisClass = new Exception().getStackTrace()[1].getClassName();
        Preferences.userNodeForPackage(thisClass).putByteArray(key, serialize(value))
    }
    
    // Boolean Perferences    
    public static String get(String key) {
        return prefsRoot.get(key, null)
    }
    
    public static String getThis(String key) {
        Class thisClass = new Exception().getStackTrace()[1].getClassName();
        return Preferences.userNodeForPackage(thisClass).get(key, null)
    }
    
    public static String get(String key, String defaultValue) {
        return prefsRoot.get(key, defaultValue)
    }
    
    public static String getThis(String key, String defaultValue) {
        Class thisClass = new Exception().getStackTrace()[1].getClassName();
        return Preferences.userNodeForPackage(thisClass).get(key, defaultValue)
    }
    
    public static void set(String key, String value) {
        prefsRoot.put(key, value)
    }
    
    public static void setThis(String key, String value) {
        Class thisClass = new Exception().getStackTrace()[1].getClassName();
        Preferences.userNodeForPackage(thisClass).put(key, value)
    }
    
    // Boolean Perferences    
    public static Boolean getBoolean(String key) {
        return prefsRoot.getBoolean(key, null)
    }
    
    public static Boolean getThisBoolean(String key) {
        Class thisClass = new Exception().getStackTrace()[1].getClassName();
        return Preferences.userNodeForPackage(thisClass).putBoolean(key, null)
    }
    
    public static Boolean getBoolean(String key, Boolean defaultValue) {
        return prefsRoot.getBoolean(key, defaultValue)
    }
    
    public static Boolean getThisBoolean(String key, Boolean defaultValue) {
        Class thisClass = new Exception().getStackTrace()[1].getClassName();
        return Preferences.userNodeForPackage(thisClass).getBoolean(key, defaultValue)
    }
    
    public static void setBoolean(String key, Boolean value) {
        prefsRoot.putBoolean(key, value)
    }
    
    public static void setThisBoolean(String key, Boolean value) {
        Class thisClass = new Exception().getStackTrace()[1].getClassName();
        Preferences.userNodeForPackage(thisClass).putBoolean(key, value)
    }    
    
    // Int Perferences    
    public static int getInt(String key) {
        return prefsRoot.getInt(key, null)
    }
    
    public static int getThisInt(String key) {
        Class thisClass = new Exception().getStackTrace()[1].getClassName();
        return Preferences.userNodeForPackage(thisClass).putInt(key, null)
    }
    
    public static int getInt(String key, int defaultValue) {
        return prefsRoot.getInt(key, defaultValue)
    }
    
    public static int getThisInt(String key, int defaultValue) {
        Class thisClass = new Exception().getStackTrace()[1].getClassName();
        return Preferences.userNodeForPackage(thisClass).getInt(key, defaultValue)
    }
    
    public static void setInt(String key, int value) {
        prefsRoot.putInt(key, value)
    }
    
    public static void setThisInt(String key, int value) {
        Class thisClass = new Exception().getStackTrace()[1].getClassName();
        Preferences.userNodeForPackage(thisClass).putInt(key, value)
    }
    
    // Remove Perferences    
    public static void remove(String key) {
        prefsRoot.remove(key)
    }
    
    public static void removeThis(String key) {
        Class thisClass = new Exception().getStackTrace()[1]
        Preferences.userNoteForPackage(thisClass).remove(key)
    }
    
    
    ///////////////////////////////    
    public static byte[] serialize(Object obj) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream()
            ObjectOutputStream os = new ObjectOutputStream(out)
            os.writeObject(obj)
            return out.toByteArray()
        } catch (All) {}
    }

    public static Object deserialize(byte[] data) {
        try {
            ByteArrayInputStream inS = new ByteArrayInputStream(data)
            ObjectInputStream is = new ObjectInputStream(inS)
            return is.readObject()
        } catch (All) {}
    }
}

