/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.th.nnl.invoice.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Thanachai.t
 */
public class InvoiceUtil {

    public static byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return out.toByteArray();
    }

    public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        return is.readObject();
    }

    public static String toBathText(String number) {
        String[] nui = {"ล้าน", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน"};
        String[] kur = {"ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า"};
        String bath = number.split("\\/.")[0];
        String result = "";

        if (number.split("\\/.").length > 1) {

        } else {
            int[] num = new int[number.length()];

            for (int i = 0; i < number.length(); i++) {
                num[i] = Integer.parseInt(number.charAt(i) + "");
            }

            if (num.length == 1) {
                result = kur[num[0]];
            } else {
                for (int i = 0; i < number.length(); i++) {
                    int nuiIndex = ((num.length - i) - 1) % 6;
                    if (num[i] != 0) {
                        if (i + 1 == number.length() && num[i] == 1 && i - 1 >= 0 && num[i - 1] != 0) {
                            result = result + "เอ็ด" + nui[nuiIndex];
                        } else if (num[i] == 1 && nuiIndex == 1) {
                            result = result + "สิบ";
                        } else if (num[i] == 2 && nuiIndex == 1) {
                            result = result + "ยี่" + nui[nuiIndex];
                        } else {
                            result = result + kur[num[i]] + nui[nuiIndex];
                        }
                        if (nuiIndex % 6 == 0) {
                            for (int j = 1; j < ((num.length - i) - 1) / 6; j++) {
                                result += nui[0];
                            }
                        }
                    }
                }
                if (num[num.length - 1] != 0) {
                    result = result.substring(0, result.length() - 4);
                }
            }

            result += "บาทถ้วน";
        }
        return result;
    }
}
