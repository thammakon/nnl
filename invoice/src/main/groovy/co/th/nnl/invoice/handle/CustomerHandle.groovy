package co.th.nnl.invoice.handle

import co.th.nnl.invoice.bean.Customer
import co.th.nnl.invoice.handle.HandleBean
import co.th.nnl.invoice.util.*
import com.mongodb.BasicDBObject
import com.mongodb.BasicDBList
import com.mongodb.DBCollection
import com.mongodb.DBObject
import com.mongodb.Mongo
import com.mongodb.MongoException
import groovy.util.logging.Slf4j
import java.util.regex.Pattern
import org.bson.types.ObjectId

/**
Customer = [id, taxid, name, address]
 */

@Slf4j
class CustomerHandle extends HandleBean<Customer> {    
               
    public Customer[] findByNameSearch(String name) {
        def list = []
        def qStr = ".*${name}.*"
        BasicDBObject basicDBObject = new BasicDBObject("name", Pattern.compile(qStr, Pattern.CASE_INSENSITIVE))
        def query = DB.find(basicDBObject).sort(new BasicDBObject("name", 1)).limit(1)
        log.info 'Result size: {}', query.size()
        if(query != null) {
            query.each({
                    list << it
                })
        }
        return list
    }
    
    public Customer[] findTextSearch(String text) {     
        def list = []
        def qStr = ".*${text}.*"
        List<BasicDBObject> l = new LinkedList<>();
        l.add(new BasicDBObject("name", Pattern.compile(qStr, Pattern.CASE_INSENSITIVE)))
        l.add(new BasicDBObject("address", Pattern.compile(qStr, Pattern.CASE_INSENSITIVE)))
        
        // Search Name
        BasicDBObject basicDBObject = new BasicDBObject('$or', l)
        def query = DB.find(basicDBObject).sort(new BasicDBObject("name", 1)).limit(100)
        log.info 'Result size: {}', query.size()
        if(query != null) {
            query.each({
                    list << it
                })
        }        
        
        return list
    }
}

