/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.th.nnl.invoice.bean.validate;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Thanachai.t
 */
public class ValidateResult {

    private List<String> errorMsg = new LinkedList<>();
    private List<String> fieldName = new LinkedList<>();
    private boolean isValid = true;

    public ValidateResult() {
    }

    public ValidateResult(String fieldName, String errorMsg) {
        if (fieldName != null && errorMsg != null) {
            isValid = false;
            this.fieldName.add(fieldName);
            this.errorMsg.add(errorMsg);
        }
    }

    public boolean isValid() {
        return isValid;
    }

    public String getFistErrorFieldName() {
        if (isValid) {
            return null;
        } else {
            return fieldName.get(0);
        }
    }

    public String getFistErrorMsg() {
        if (isValid) {
            return null;
        } else {
            return errorMsg.get(0);
        }
    }

    public ValidateResult append(String fieldName, String errorMsg) {
        if (fieldName != null && errorMsg != null) {
            isValid = false;
            this.fieldName.add(fieldName);
            this.errorMsg.add(errorMsg);
        }
        return this;
    }

    public ValidateResult addAll(ValidateResult validateResult) {
        if (!validateResult.isValid()) {
            isValid = false;
            fieldName.addAll(validateResult.fieldName);
            errorMsg.addAll(validateResult.errorMsg);
        }
        return this;
    }
    
}
