/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.th.nnl.invoice.bean.validate;

import co.th.nnl.invoice.bean.Customer;

/**
 *
 * @author Thanachai.t
 */
public class CustomerValidate {

    private Customer customer;

    public CustomerValidate(Customer customer) {
        this.customer = customer;
    }

    public ValidateResult validateAll() {
        ValidateResult result = new ValidateResult();
        result.append("name", validName(customer.getName()))
                .append("address", validAddress(customer.getAddress()));
        return result;
    }

    public static String validName(String name) {
        if (name == null || name.equals("")) {
            return "ชื่อลูกค้ามีค่าว่าง";
        }
        return null;
    }

    public static String validAddress(String address) {
        if (address == null || address.equals("")) {
            return "ทีอยู่ลูกค้ามีค่าว่าง";
        }
        return null;
    }
}
