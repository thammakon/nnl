/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.th.nnl.invoice.bean.validate;

import co.th.nnl.invoice.bean.Invoice;

/**
 *
 * @author Thanachai.t
 */
public class InvoiceValidate {

    private Invoice invoice;

    public InvoiceValidate(Invoice invoice) {
        this.invoice = invoice;
    }

    public ValidateResult validateAll() {
        ValidateResult result = new ValidateResult();
        result
                .append("invoiceId", validInvoiceId(invoice.getInvoiceId()))
                .append("invoiceDate", validInvoiceDate(invoice.getInvoiceDate()))
                .append("remark", validRemark(invoice.getRemark()))
                .append("sellId", validSellId(invoice.getSellId()))
                .append("itemId", validItemId(invoice.getItemId()))
                .append("itemDescriptions", validItemDescriptions(invoice.getItemDescriptions()))
                .append("itemQuantity", validItemQuantity(invoice.getItemQuantity()))
                .append("itemUnitPrice", validItemUnitPrice(invoice.getItemUnitPrice()));
        return result;
    }

    public static String validInvoiceId(String invoiceId) {
        if (invoiceId == null || invoiceId.equals("")) {
            return "เลขที่ใบแจ้งหนี้มีค่าว่าง";
        } 
        if (!invoiceId.replaceFirst("NT[0-9]{4}\\/[0-9]{5}", "").equals("")) {
            return "เลขที่ใบแจ้งหนี้ ไม่ถูกต้อง";
        }
        return null;
    }

    public static String validInvoiceDate(String invoiceDate) {
        if (invoiceDate == null || invoiceDate.equals("")) {
            return "ข้อมูลวันที่มีค่าว่าง";
        }
        return null;
    }

    public static String validRemark(String remark) {
        return null;
    }

    public static String validSellId(String sellId) {
        if (sellId == null || sellId.equals("")) {
            return "ใบสั่งซื้อเลขที่/ใบสั่งงาน มีค่าว่าง";
        }
        return null;
    }

    public static String validItemId(String[] itemId) {
        return null;
    }

    public static String validItemDescriptions(String[] itemDescriptions) {
        return null;
    }

    public static String validItemQuantity(String[] itemQuantity) {
        return null;
    }

    public static String validItemUnitPrice(String[] itemUnitPrice) {
        return null;
    }

}
