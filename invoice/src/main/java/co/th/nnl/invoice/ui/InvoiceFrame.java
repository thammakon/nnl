package co.th.nnl.invoice.ui;

import co.th.nnl.invoice.bean.*;
import co.th.nnl.invoice.bean.validate.CustomerValidate;
import co.th.nnl.invoice.bean.validate.InvoiceValidate;
import co.th.nnl.invoice.bean.validate.ValidateResult;
import co.th.nnl.invoice.handle.*;
import co.th.nnl.invoice.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.text.MaskFormatter;
import org.bson.types.ObjectId;
import static co.th.nnl.invoice.util.ErrorHandle.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayDeque;

/**
 *
 * @author Thanachai.t
 */
public class InvoiceFrame extends javax.swing.JFrame {

    static Logger log = Logger.getLogger(InvoiceFrame.class.getName());

    ObjectId customerId;
    private ObjectId _id;
    private SimpleDateFormat sdf;
    private TableModel itemListTableModel;
    private static InvoiceFrame instance;
    private static MaskFormatter dateMask;
    private static MaskFormatter invoiceIdMask;
    private InvoiceHandle invoiceHandle;
    private ArrayDeque<String> historySellId;
    private ArrayDeque<String> historyRemark;
    private ArrayDeque<String> historyCustomerName;
    private Thread t;
    private int customerSearching = 0;
    private Customer[] resultSearchCustomer;
    private long flagThread;

    {
        try {
            historySellId = (ArrayDeque<String>) InvoicePerferences.getThisObject("historySellId");
            historyRemark = (ArrayDeque<String>) InvoicePerferences.getThisObject("historyRemark");
            historyCustomerName = (ArrayDeque<String>) InvoicePerferences.getThisObject("historyCustomerName");
            if (historySellId == null) {
                historySellId = new ArrayDeque<String>();
            }
            if (historyRemark == null) {
                historyRemark = new ArrayDeque<String>();
            }
            if (historyCustomerName == null) {
                historyCustomerName = new ArrayDeque<String>();
            }
        } catch (Exception ex) {
            Logger.getLogger(InvoiceFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = historySellId.size(); i < 15; i++) {
            historySellId.add("");
        }
        for (int i = historyRemark.size(); i < 15; i++) {
            historyRemark.add("");
        }
        for (int i = historyCustomerName.size(); i < 15; i++) {
            historyCustomerName.add("");
        }

        try {
            dateMask = new MaskFormatter("##/##/####");
            dateMask.setPlaceholderCharacter('_');
            invoiceIdMask = new MaskFormatter("NT####/#####");
            invoiceIdMask.setPlaceholderCharacter('_');
        } catch (ParseException ex) {
            Logger.getLogger(InvoiceFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setStatusBar(String msg) {
        lbStatus.setText(msg);
        pack();
    }

    public static InvoiceFrame getInstance() {
        if (instance == null) {
            instance = new InvoiceFrame();
            if (InvoicePerferences.getBoolean("noDb", true)) {
                instance.setStatusBar("ไม่ใช้ฐานข้อมูล");
            } else {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        InvoiceFrame.getInstance().connectToDatabase();
                    }
                }).start();
            }

            // Set Center Screen
            instance.setLocationRelativeTo(null);
        }
        return instance;
    }

    public void connectToDatabase() {
        if (InvoicePerferences.getBoolean("noDb", true)) {
            ftInvoiceId.setText("NT" + sdf.format(new Date()));
        } else {
            try {
                setStatusBar("กำลังเชื่อมต่อฐานข้อมูล...");
                invoiceHandle = new InvoiceHandle();
                ftInvoiceId.setText("NT" + sdf.format(new Date()) + invoiceHandle.getNextInvoiceId());
                setStatusBar("เชื่อมต่อฐานข้อมูลสำเร็จ");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, getErrorMessage(ex), "ผิดพลาด", JOptionPane.ERROR_MESSAGE);
                ftInvoiceId.setText("NT" + sdf.format(new Date()));
                setStatusBar("เชื่อมต่อฐานข้อมูลล้วเหลว");
            }
        }
    }

    /**
     * Class data simple for develop
     */
    public class TestData {

        public TestData() {
//            txtCustomerName.setSelectedItem("บริษัท เอ็น นิวทรัล โลจิสติกส์ จำกัด");
            taCustomerAddress.setText("เลขที่ 3  ซอยสีหบุรานุกิจ  17  แขวงมีนบุรี\n เขตมีนบุรี กรุงเทพมหานคร 10510");
            ftDate.setText("18/10/2015");
            txtRemark.setSelectedItem("TAX ID : 0105541069148   ( สำนักงานใหญ่  )    ");
            txtSellId.setSelectedItem("BOOKING NO.งาน SHIN PAPER");
            itemListTableModel = new DefaultTableModel(
                    new Object[][]{
                        {"8/19/1958", "ค่าขนส่ง UNITHAI  -  บางแค -   YJC           1X40\\'HQ.", "1", "5500"},
                        {"", "ค่าขนส่ง PAT   -  บางแค -   YJC                  2X40\\'HQ.", "1", "976"},
                        {"8/20/1958", "ค่าคืนตู้เปล่า", "2", "5000"},
                        {"", "1.SKHU - 8717223", "2", "976"},
                        {"", "2.GATU - 8738047", "3", "1234"},
                        {"", "3.SKHU - 8701675", "3", "5678"},
                        {"", "", "", ""},
                        {"", "", "", ""},
                        {"", "", "", ""},
                        {"", "", "", ""}
                    },
                    new String[]{
                        "ลำดับ", "รายการ", "จำนวนหน่วย", "หน่วยละ"
                    }
            );
            tbItemList.setModel(itemListTableModel);
            tbItemList.getTableHeader().setReorderingAllowed(false);
            tbItemList.getTableHeader().getColumnModel().getColumn(0).setWidth(WIDTH);
        }
    }

    private void initInvoiceForm() {
        Date currentDate = new Date();
        txtCustomerName.setSelectedItem("");
        taCustomerAddress.setText("");
        txtRemark.setSelectedItem("");
        txtSellId.setSelectedItem("");
        sdf = new SimpleDateFormat("dd/M/yyyy");
        ftDate.setText(sdf.format(currentDate));
        sdf = new SimpleDateFormat("yyyy/M");

        if (InvoicePerferences.getBoolean("noDb", true)) {
            try {
                int id = Integer.parseInt(ftInvoiceId.getText().substring(3));
                id++;
                ftInvoiceId.setText("NT" + sdf.format(new Date()) + id);
            } catch (Exception ex) {
                ex.printStackTrace();
                ftInvoiceId.setText("NT" + sdf.format(new Date()));
            }
        } else {
            try {
                ftInvoiceId.setText("NT" + sdf.format(new Date()) + invoiceHandle.getNextInvoiceId());
            } catch (Exception ex) {
                ftInvoiceId.setText("NT" + sdf.format(new Date()));
            }
        }

        if (customerId != null) {
            txtCustomerName.setEnabled(false);
            taCustomerAddress.setEnabled(false);
            btnCustomerEdit.setEnabled(true);
        }

        itemListTableModel = new DefaultTableModel(
                new Object[][]{
                    {"", "", "", ""},
                    {"", "", "", ""},
                    {"", "", "", ""},
                    {"", "", "", ""},
                    {"", "", "", ""},
                    {"", "", "", ""},
                    {"", "", "", ""},
                    {"", "", "", ""},
                    {"", "", "", ""},
                    {"", "", "", ""}
                },
                new String[]{
                    "ลำดับ", "รายการ", "จำนวนหน่วย", "หน่วยละ"
                }
        );
        tbItemList.setModel(itemListTableModel);
        tbItemList.getTableHeader().setReorderingAllowed(false);
    }

    /**
     * Creates new form InvoiceFrame
     */
    private InvoiceFrame() {
        initComponents();
        initInvoiceForm();
/*
        txtCustomerName.getEditor().getEditorComponent().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (!InvoicePerferences.getBoolean("noDb", true)
                        && txtCustomerName.getEditor().getItem().toString().length() >= 3) {
                    if (t == null) {
                        t = new Thread(() -> {
                            while (true) {
                                String tmp = txtCustomerName.getEditor().getItem().toString();
                                try {
                                    Thread.sleep(1000L);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(InvoiceFrame.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                String text = txtCustomerName.getEditor().getItem().toString();
                                if (text.equals(tmp)) {
                                    System.out.println("search...");

                                    long thisThread = System.currentTimeMillis();
                                    flagThread = thisThread;
                                    customerSearching++;
                                    resultSearchCustomer = new CustomerHandle().findByNameSearch(text);
                                    if (flagThread == thisThread
                                            && resultSearchCustomer.length > 0) {
                                        String list[] = new String[resultSearchCustomer.length];
                                        for (int i = 0; i < resultSearchCustomer.length; i++) {
                                            list[i] = resultSearchCustomer[i].getName();
                                        }

                                        txtCustomerName.setModel(new javax.swing.DefaultComboBoxModel(list));
                                        txtCustomerName.getEditor().setItem(text);
                                        txtCustomerName.showPopup();
                                    }
                                    customerSearching--;
                                    // Exit while loop
                                    break;
                                }
                            }
//                            t = null;
                        });
                    } else if (!t.isAlive()) {
                        t.start();
                    }
                }
            }
        }
        );
*/
        if (InvoicePerferences.getBoolean(
                "isModeDev", false)) {
            new TestData();
        }
    }

    private boolean validationInputData(Invoice invoice, Customer customer) {
        ValidateResult result = new CustomerValidate(customer).validateAll();
        result.addAll(new InvoiceValidate(invoice).validateAll());
        if (!result.isValid()) {
            JOptionPane.showMessageDialog(this, result.getFistErrorMsg(), "ข้อมูลไม่ถูกต้อง", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbItemList = new javax.swing.JTable();
        btnSave = new javax.swing.JButton();
        btnClearForm = new javax.swing.JToggleButton();
        jButton4 = new javax.swing.JButton();
        chkPrint = new javax.swing.JCheckBox();
        chkReview = new javax.swing.JCheckBox();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        taCustomerAddress = new javax.swing.JTextArea();
        btnCustomerSearch = new javax.swing.JButton();
        btnCustomerEdit = new javax.swing.JButton();
        txtCustomerName = new javax.swing.JComboBox<String>();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        jLabel4 = new javax.swing.JLabel();
        ftInvoiceId = new javax.swing.JFormattedTextField(invoiceIdMask);
        ftDate = new javax.swing.JFormattedTextField(dateMask);
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtSellId = new javax.swing.JComboBox<String>();
        txtRemark = new javax.swing.JComboBox<String>();
        chkAutoClearForm = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        lbStatus = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ใบแจ้งหนี้");
        addWindowStateListener(new java.awt.event.WindowStateListener() {
            public void windowStateChanged(java.awt.event.WindowEvent evt) {
                formWindowStateChanged(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowDeactivated(java.awt.event.WindowEvent evt) {
                formWindowDeactivated(evt);
            }
            public void windowDeiconified(java.awt.event.WindowEvent evt) {
                formWindowDeiconified(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        jScrollPane1.setPreferredSize(new java.awt.Dimension(453, 131));

        initInvoiceForm();
        tbItemList.setModel(itemListTableModel);
        tbItemList.setToolTipText("");
        tbItemList.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tbItemList.setNextFocusableComponent(btnSave);
        tbItemList.setRowSelectionAllowed(false);
        tbItemList.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tbItemList);
        tbItemList.getAccessibleContext().setAccessibleName("");

        btnSave.setText("บันทึก");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClearForm.setText("เคลียร์");
        btnClearForm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearFormActionPerformed(evt);
            }
        });

        jButton4.setText("ลบออกจากระบบ");
        jButton4.setEnabled(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        chkPrint.setText("พิมพ์");
        chkPrint.setEnabled(InvoicePerferences.getBoolean("chkPrint.isEnabled", false));
        chkPrint.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                chkPrintStateChanged(evt);
            }
        });
        chkPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkPrintActionPerformed(evt);
            }
        });

        chkReview.setSelected(InvoicePerferences.getBoolean("chkReview.isSelected", true));
        chkReview.setText("ดูตัวอย่าง");
        chkReview.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                chkReviewStateChanged(evt);
            }
        });
        chkReview.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkReviewActionPerformed(evt);
            }
        });

        jLayeredPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLayeredPane1.setName("ewa t"); // NOI18N
        jLayeredPane1.setPreferredSize(new java.awt.Dimension(400, 200));
        jLayeredPane1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jLayeredPane1KeyReleased(evt);
            }
        });

        jLabel1.setText("นามลูกค้า :");

        jLabel2.setText("ที่อยู่ :");

        taCustomerAddress.setColumns(20);
        taCustomerAddress.setRows(3);
        jScrollPane2.setViewportView(taCustomerAddress);

        btnCustomerSearch.setText("ค้นหา");
        btnCustomerSearch.setDoubleBuffered(true);
        btnCustomerSearch.setFocusable(false);
        btnCustomerSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomerSearchActionPerformed(evt);
            }
        });

        btnCustomerEdit.setText("แก้ไข");
        btnCustomerEdit.setEnabled(false);
        btnCustomerEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomerEditActionPerformed(evt);
            }
        });

        txtCustomerName.setEditable(true);
        txtCustomerName.setMaximumRowCount(4);
        txtCustomerName.setModel(new javax.swing.DefaultComboBoxModel(historyCustomerName.toArray()));

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addComponent(txtCustomerName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCustomerSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCustomerEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE))
                .addContainerGap())
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(btnCustomerSearch)
                    .addComponent(btnCustomerEdit)
                    .addComponent(txtCustomerName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        jLayeredPane1.setLayer(jLabel1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jLabel2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jScrollPane2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(btnCustomerSearch, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(btnCustomerEdit, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(txtCustomerName, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLayeredPane2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLayeredPane2.setPreferredSize(new java.awt.Dimension(400, 200));

        jLabel4.setText("เลขที่ใบแจ้งหนี้ :");

        ftDate.setText("");

        jLabel5.setText("วันที่ :");

        jLabel7.setText("ใบสั่งซื้อเลขที่/ใบสั่งงาน :");

        jLabel3.setText("หมายเหตุ :");

        txtSellId.setEditable(true);
        txtSellId.setModel(new javax.swing.DefaultComboBoxModel(historySellId.toArray()));
        txtSellId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSellIdActionPerformed(evt);
            }
        });

        txtRemark.setEditable(true);
        txtRemark.setModel(new javax.swing.DefaultComboBoxModel(historyRemark.toArray()));
        txtRemark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRemarkActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jLayeredPane2Layout = new javax.swing.GroupLayout(jLayeredPane2);
        jLayeredPane2.setLayout(jLayeredPane2Layout);
        jLayeredPane2Layout.setHorizontalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jLayeredPane2Layout.createSequentialGroup()
                        .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(jLabel5))
                            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                .addGap(51, 51, 51)
                                .addComponent(jLabel4))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel7)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(ftInvoiceId)
                            .addComponent(ftDate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                            .addComponent(txtSellId, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jLayeredPane2Layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtRemark, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jLayeredPane2Layout.setVerticalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(ftInvoiceId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ftDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSellId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRemark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap())
        );
        jLayeredPane2.setLayer(jLabel4, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(ftInvoiceId, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(ftDate, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel5, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel7, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(txtSellId, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(txtRemark, javax.swing.JLayeredPane.DEFAULT_LAYER);

        chkAutoClearForm.setSelected(InvoicePerferences.getBoolean("chkAutoClearForm.isSelected", false));
        chkAutoClearForm.setText("เคลียร์ฟอร์มหลังจากบันทึก");
        chkAutoClearForm.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                chkAutoClearFormStateChanged(evt);
            }
        });
        chkAutoClearForm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAutoClearFormActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel1.setPreferredSize(new java.awt.Dimension(57, 16));

        lbStatus.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel8.setText("สถานะ :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel8)
                .addGap(12, 12, 12)
                .addComponent(lbStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
                    .addComponent(jLabel8))
                .addGap(2, 2, 2))
        );

        jMenu1.setText("ตั่งค่า");
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        jMenuItem1.setText("ฐานข้อมูล");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("ธีม");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("ฐานข้อมูล");

        jMenuItem3.setText("ลูกค้า");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuItem4.setText("ใบแจ้งหนี้");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLayeredPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 526, Short.MAX_VALUE)
                        .addGap(12, 12, 12)
                        .addComponent(jLayeredPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 542, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnClearForm)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chkAutoClearForm)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 528, Short.MAX_VALUE)
                        .addComponent(chkReview)
                        .addGap(12, 12, 12)
                        .addComponent(chkPrint)
                        .addGap(12, 12, 12)
                        .addComponent(btnSave))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1104, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLayeredPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                    .addComponent(jLayeredPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClearForm)
                    .addComponent(jButton4)
                    .addComponent(btnSave)
                    .addComponent(chkPrint)
                    .addComponent(chkReview)
                    .addComponent(chkAutoClearForm))
                .addGap(12, 12, 12)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
//        if (JOptionPane.showConfirmDialog(this, "Are you want to permanently delete this invoice?", "Delete this Invoice.", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
//            String errorText = invoiceHandle.remove(_id);
//            if (errorText == null) {
//                JOptionPane.showMessageDialog(this, "Succeed.");
//                initInvoiceForm();
//            } else {
//                JOptionPane.showMessageDialog(this, errorText, "Failed", JOptionPane.ERROR_MESSAGE);
//            }
//        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        try {
            boolean isSaveSeccess = false;
            btnSave.setEnabled(false);

            Invoice invoice = new Invoice();
            Customer customer = new Customer();

            String[] itemId = new String[10];
            String[] descriptions = new String[10];
            String[] quantity = new String[10];
            String[] itemUnitPrice = new String[10];

            // get data form table
            for (int row = 0; row < 10; row++) {
                itemId[row] = tbItemList.getValueAt(row, 0).toString().trim();
                descriptions[row] = tbItemList.getValueAt(row, 1).toString().trim();
                quantity[row] = tbItemList.getValueAt(row, 2).toString().trim();
                itemUnitPrice[row] = tbItemList.getValueAt(row, 3).toString().trim();
            }

            customer.setId(customerId);
            customer.setName(txtCustomerName.getSelectedItem().toString().trim());
            customer.setAddress(taCustomerAddress.getText().trim());

            invoice.setId(_id);
            invoice.setCustomer_id(customerId);
            invoice.setInvoiceId(ftInvoiceId.getText().trim());
            invoice.setInvoiceDate(ftDate.getText().trim());
            invoice.setRemark(txtRemark.getSelectedItem().toString().trim());
            invoice.setSellId(txtSellId.getSelectedItem().toString().trim());
            invoice.setItemId(itemId);
            invoice.setItemDescriptions(descriptions);
            invoice.setItemQuantity(quantity);
            invoice.setItemUnitPrice(itemUnitPrice);

            if (validationInputData(invoice, customer)) {
                keepHistory();

                ////////////////////////////////////////////////////////////////
                if (InvoicePerferences.getBoolean("noDb", true)) {
                    // Not use Database Mode
                } else {
                    // Use Database Mode

                    instance.setStatusBar("กำลังบันทึก...");
                    try {
                        if (customerId == null || !btnCustomerEdit.isEnabled()) {
                            customerId = new CustomerHandle().save(customer);
                            JOptionPane.showMessageDialog(this, "บันทึกข้อมูลลูกค้าสำเร็จ");
                            btnCustomerEdit.setEnabled(true);
                            txtCustomerName.setEnabled(false);
                            taCustomerAddress.setEditable(false);
                        }
                        invoice.setCustomer_id(customerId);

                        _id = invoiceHandle.save(invoice);

                        JOptionPane.showMessageDialog(this, "บันทึกใบแจ้งหนี้สำเร็จ");

                        lbStatus.setText("บันทึกสำเร็จ");

                        try {
                            ftInvoiceId.setText("NT" + sdf.format(new Date()) + invoiceHandle.getNextInvoiceId());
                        } catch (Exception e) {
                            ftInvoiceId.setText("NT" + sdf.format(new Date()));
                        }
                        isSaveSeccess = true;
                    } catch (Exception ex) {
                        setStatusBar("บันทึกไม่สำเร็จ");
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(this, getErrorMessage(ex), "บันทึกไม่สำเร็จ", JOptionPane.ERROR_MESSAGE);
                    }
                }

                if (InvoicePerferences.getBoolean("noDb", true) || isSaveSeccess) {
                    if (chkReview.isSelected()) {
                        setStatusBar("กำลังเป็ดไฟล์...");
                        new InvoiceExcel(invoice, customer).open();
                        setStatusBar("");
                    } else if (chkPrint.isSelected()) {
                        setStatusBar("กำลังพิมพ์...");
                        new InvoiceExcel(invoice, customer).print();
                        setStatusBar("");
                    }
                }

                if (chkAutoClearForm.isSelected()) {
                    initInvoiceForm();
                }

                if (customerId != null) {
                    txtCustomerName.setEnabled(false);
                    taCustomerAddress.setEnabled(false);
                    btnCustomerEdit.setEnabled(true);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            btnSave.setEnabled(true);
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnClearFormActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearFormActionPerformed
        initInvoiceForm();
    }//GEN-LAST:event_btnClearFormActionPerformed

    private void chkPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkPrintActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkPrintActionPerformed

    private void btnCustomerSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustomerSearchActionPerformed
        CustomerSearch.getInstance().setVisible(true);
        if (customerId != null) {
            txtCustomerName.setEditable(false);
            taCustomerAddress.setEditable(false);
            btnCustomerEdit.setEnabled(true);
        }
    }//GEN-LAST:event_btnCustomerSearchActionPerformed

    private void chkReviewStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_chkReviewStateChanged
        if (chkReview.isSelected()) {
            chkPrint.setEnabled(false);
        } else {
            chkPrint.setEnabled(true);
        }
    }//GEN-LAST:event_chkReviewStateChanged

    private void chkReviewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkReviewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkReviewActionPerformed

    private void btnCustomerEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustomerEditActionPerformed
        txtCustomerName.setEnabled(true);
        taCustomerAddress.setEnabled(true);
        btnCustomerEdit.setEnabled(false);
    }//GEN-LAST:event_btnCustomerEditActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try {
            // Set Perferences
            InvoicePerferences.setBoolean("chkAutoClearForm.isSelected", chkAutoClearForm.isSelected());
            InvoicePerferences.setBoolean("chkPrint.isSelected", chkPrint.isSelected());
            InvoicePerferences.setBoolean("chkReview.isSelected", chkReview.isSelected());

            InvoicePerferences.setThisObject("historySellId", (Object) historySellId);
            InvoicePerferences.setThisObject("historyRemark", (Object) historyRemark);
            InvoicePerferences.setThisObject("historyCustomerName", (Object) historyCustomerName);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_formWindowClosing

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        ManageDbDialog.getInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void chkAutoClearFormActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAutoClearFormActionPerformed

    }//GEN-LAST:event_chkAutoClearFormActionPerformed

    private void chkAutoClearFormStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_chkAutoClearFormStateChanged

    }//GEN-LAST:event_chkAutoClearFormStateChanged

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void formWindowDeactivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowDeactivated

    }//GEN-LAST:event_formWindowDeactivated

    private void formWindowDeiconified(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowDeiconified

    }//GEN-LAST:event_formWindowDeiconified

    private void formWindowStateChanged(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowStateChanged

    }//GEN-LAST:event_formWindowStateChanged

    private void chkPrintStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_chkPrintStateChanged

    }//GEN-LAST:event_chkPrintStateChanged

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        new ConfigLookAndFeel().setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void txtSellIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSellIdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSellIdActionPerformed

    private void txtRemarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRemarkActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRemarkActionPerformed

    private void jLayeredPane1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLayeredPane1KeyReleased

    }//GEN-LAST:event_jLayeredPane1KeyReleased

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        System.out.println("frame KR");
    }//GEN-LAST:event_formKeyReleased

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        CustomerSearch.getInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void keepHistory() {
        if (!historySellId.contains(txtSellId.getSelectedItem().toString().trim())) {
            historySellId.addFirst(txtSellId.getSelectedItem().toString().trim());
            historySellId.removeLast();
        } else {
            historySellId.remove(txtSellId.getSelectedItem().toString().trim());
            historySellId.addFirst(txtSellId.getSelectedItem().toString().trim());
        }
        txtSellId.setModel(new javax.swing.DefaultComboBoxModel(historySellId.toArray()));

        if (!historyRemark.contains(txtRemark.getSelectedItem().toString().trim())) {
            historyRemark.addFirst(txtRemark.getSelectedItem().toString().trim());
            historyRemark.removeLast();
        } else {
            historyRemark.remove(txtRemark.getSelectedItem().toString().trim());
            historyRemark.addFirst(txtRemark.getSelectedItem().toString().trim());
        }
        txtRemark.setModel(new javax.swing.DefaultComboBoxModel(historyRemark.toArray()));

        if (!historyCustomerName.contains(txtCustomerName.getSelectedItem().toString().trim())) {
            historyCustomerName.addFirst(txtCustomerName.getSelectedItem().toString().trim());
            historyCustomerName.removeLast();
        } else {
            historyCustomerName.remove(txtCustomerName.getSelectedItem().toString().trim());
            historyCustomerName.addFirst(txtCustomerName.getSelectedItem().toString().trim());
        }
        txtCustomerName.setModel(new javax.swing.DefaultComboBoxModel(historyCustomerName.toArray()));
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btnClearForm;
    private javax.swing.JButton btnCustomerEdit;
    public javax.swing.JButton btnCustomerSearch;
    private javax.swing.JButton btnSave;
    private javax.swing.JCheckBox chkAutoClearForm;
    private javax.swing.JCheckBox chkPrint;
    private javax.swing.JCheckBox chkReview;
    private javax.swing.JFormattedTextField ftDate;
    private javax.swing.JFormattedTextField ftInvoiceId;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JLayeredPane jLayeredPane2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JLabel lbStatus;
    public javax.swing.JTextArea taCustomerAddress;
    private javax.swing.JTable tbItemList;
    public javax.swing.JComboBox txtCustomerName;
    private javax.swing.JComboBox txtRemark;
    private javax.swing.JComboBox txtSellId;
    // End of variables declaration//GEN-END:variables
}


/*


 if (!tmp.equals(text)
 && text.length() >= 3
 && !InvoicePerferences.getBoolean("noDb", true)) {

 if (customerSearching <= 5) {
 new Thread(new Runnable() {
 public void run() {
 // Set delay time
 String text = txtCustomerName.getSelectedItem().toString().trim();
 System.out.println("Customer Search: " + text);
 System.out.println(customerSearching);
 long thisThread = System.currentTimeMillis();
 flagThread = thisThread;
 customerSearching++;
 resultSearchCustomer = new CustomerHandle().findByNameSearch(text);
 if (flagThread == thisThread) {
 String list[] = new String[resultSearchCustomer.length + 1];

 System.out.println("list size: " + list.length);

 list[0] = text;
 for (int i = 0; i < resultSearchCustomer.length; i++) {
 list[i + 1] = resultSearchCustomer[i].getName();
 }

 txtCustomerName.setModel(new javax.swing.DefaultComboBoxModel(list));
 }
 customerSearching--;
 }
 }).start();
 }

 */
