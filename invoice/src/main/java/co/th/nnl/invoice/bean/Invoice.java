/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.th.nnl.invoice.bean;

import com.mongodb.BasicDBObject;
import org.bson.types.ObjectId;

/**
 *
 * @author Thanachai.t
 */
public class Invoice extends BasicDBObject {

    public Invoice() {}

    public Invoice(ObjectId _id, ObjectId customer_id, String remark, String sellId, String invoiceId, String invoiceDate, String[] itemId, String[] itemDescriptions, String[] itemQuantity, String[] itemUnitPrice) {
        super.put("_id", _id);
        super.put("customer_id", customer_id);
        super.put("remark", remark);
        super.put("sellId", sellId);
        super.put("invoiceId", invoiceId);
        super.put("invoiceDate", invoiceDate);
        super.put("itemId", itemId);
        super.put("itemDescriptions", itemDescriptions);
        super.put("itemQuantity", itemQuantity);
        super.put("itemUnitPrice", itemUnitPrice);
    }   

    public Invoice(String sellId, String invoiceDate, ObjectId customer_id, String remark, String invoiceId, String[] itemId, String[] itemDescriptions, String[] itemQuantity, String[] itemUnitPrice) {
        super.put("customer_id", customer_id);
        super.put("remark", remark);
        super.put("sellId", sellId);
        super.put("invoiceId", invoiceId);
        super.put("invoiceDate", invoiceDate);
        super.put("itemId", itemId);
        super.put("itemDescriptions", itemDescriptions);
        super.put("itemQuantity", itemQuantity);
        super.put("itemUnitPrice", itemUnitPrice);
    }

    public Invoice(String sellId,String invoiceDate,  String remark, String invoiceId, String[] itemId, String[] itemDescriptions, String[] itemQuantity, String[] itemUnitPrice) {
        super.put("remark", remark);
        super.put("sellId", sellId);
        super.put("invoiceId", invoiceId);
        super.put("invoiceDate", invoiceDate);
        super.put("itemId", itemId);
        super.put("itemDescriptions", itemDescriptions);
        super.put("itemQuantity", itemQuantity);
        super.put("itemUnitPrice", itemUnitPrice);
    }
    
    public ObjectId getId() {
        return (ObjectId)super.get("_id");
    }

    public void setId(ObjectId _id) {
        super.put("_id", _id);
    }

    public ObjectId getCustomer_id() {
        return super.getObjectId("customer_id");
    }

    public void setCustomer_id(ObjectId customer_id) {
        super.put("customer_id", customer_id);
    }

    public String getRemark() {
        return super.getString("remark");
    }

    public void setRemark(String remark) {
        super.put("remark", remark);
    }

    public String getSellId() {
        return super.getString("sellId");
    }

    public void setSellId(String sellId) {
        super.put("sellId", sellId);
    }

    public String getInvoiceId() {
        return super.getString("invoiceId");
    }

    public void setInvoiceId(String invoiceId) {
        super.put("invoiceId", invoiceId);
    }

    public String getInvoiceDate() {
        return super.getString("invoiceDate");
    }

    public void setInvoiceDate(String invoiceDate) {
        super.put("invoiceDate", invoiceDate);
    }

    public String[] getItemId() {
        return (String[])super.get("itemId");
    }

    public void setItemId(String[] itemId) {
        super.put("itemId", itemId);
    }

    public String[] getItemDescriptions() {
        return (String[])super.get("itemDescriptions");
    }

    public void setItemDescriptions(String[] itemDescriptions) {
        super.put("itemDescriptions", itemDescriptions);
    }

    public String[] getItemQuantity() {
        return (String[])super.get("itemQuantity");
    }

    public void setItemQuantity(String[] itemQuantity) {
        super.put("itemQuantity", itemQuantity);
    }

    public String[] getItemUnitPrice() {
        return (String[])super.get("itemUnitPrice");
    }

    public void setItemUnitPrice(String[] itemUnitPrice) {
        super.put("itemUnitPrice", itemUnitPrice);
    }
    
    
}
