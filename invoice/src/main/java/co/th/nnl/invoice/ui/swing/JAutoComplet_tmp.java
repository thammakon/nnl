/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.th.nnl.invoice.ui.swing;

import javax.swing.*;
import org.apache.commons.collections.Buffer;
import org.apache.commons.collections.buffer.CircularFifoBuffer;
import co.th.nnl.invoice.util.*;

/**
 *
 * @author Thanachai.t
 */
public class JAutoComplet_tmp extends JComboBox<String> {

    private JComboBox jComboBox;
    private Buffer historyList;
    private String text = "";

    {
        historyList = new CircularFifoBuffer(5);
    }

    private JAutoComplet_tmp() {
        jComboBox = new javax.swing.JComboBox();
        jComboBox.setEditable(true);

        historyList.add("1");
        historyList.add("2");
        historyList.add("3");
        historyList.add("4");
        historyList.add("5");
        historyList.add("6");
        historyList.add("7");

        String[] historys = new String[5];

        historys[0] = (String) historyList.toArray()[4];
        historys[1] = (String) historyList.toArray()[3];
        historys[2] = (String) historyList.toArray()[2];
        historys[3] = (String) historyList.toArray()[1];
        historys[4] = (String) historyList.toArray()[0];

        // set list
        jComboBox.setModel(new javax.swing.DefaultComboBoxModel(historys));
    }

    public static JComboBox getObject() {
        return new JAutoComplet_tmp().build();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    private JComboBox build() {
        return jComboBox;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

}


/*



 jComboBox2 = new javax.swing.JComboBox();

 jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
 */
