/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.th.nnl.invoice.bean;

import com.mongodb.BasicDBObject;
import org.bson.types.ObjectId;

/**
 *
 * @author Thanachai.t
 */
public class Customer extends BasicDBObject {

    public Customer() {}

    public Customer(String name, String address) {
        super.put("name", name);
        super.put("address", address);
    }
    
    public Customer(ObjectId _id, String name, String address) {
        super.put("_id", _id);
        super.put("name", name);
        super.put("address", address);
    }
        

    public ObjectId getId() {
        return (ObjectId)super.get("_id");
    }

    public void setId(ObjectId _id) {
        super.put("_id", _id);
    }

    public String getName() {
        return super.getString("name");
    }

    public void setName(String name) {
        super.put("name", name);
    }

    public String getAddress() {
        return super.getString("address");
    }

    public void setAddress(String address) {
        super.put("address", address);
    }
    
    
}
